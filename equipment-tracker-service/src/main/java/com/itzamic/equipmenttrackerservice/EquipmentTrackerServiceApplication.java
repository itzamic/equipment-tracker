package com.itzamic.equipmenttrackerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquipmentTrackerServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(EquipmentTrackerServiceApplication.class, args);
  }
}
